<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../resources/_prePage.jsp"/>

<p class="font" style="margin: 0px">TASK CREATE</p>

<form:form method="post" action="/tasks/create" modelAttribute="task">
    <div style="margin-top: 5px">Enter task name:</div>
    <input type="text" name="name" value="name"/><br/>
    <div style="margin-top: 5px">Enter task description:</div>
    <input type="text" name="description" value="description"/><br/>
    <div style="margin-top: 5px">Choose project for this task:</div>
    <select name="projectId">
        <c:forEach items="${projects}" var="project">
            <option value=<c:out value="${project.id}"/>><c:out value="${project.name}"/></option>
        </c:forEach>
    </select>
    <div>
        <input class="button" style="margin-top: 10px" type="submit" value="Create task"/>
    </div>
</form:form>

<jsp:include page="../resources/_postPage.jsp"/>