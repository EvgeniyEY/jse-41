package ru.ermolaev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Session;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class SessionDTO extends AbstractEntityDTO implements Cloneable {

    private Long startTime;

    private String userId;

    private String signature;

    @Nullable
    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Nullable
    public static SessionDTO toDTO(@Nullable final Session session) {
        if (session == null) return null;
        return new SessionDTO(session);
    }

    @NotNull
    public static List<SessionDTO> toDTO(@Nullable final Collection<Session> sessions) {
        if (sessions == null || sessions.isEmpty()) return Collections.emptyList();
        @NotNull final List<SessionDTO> result = new ArrayList<>();
        for (@Nullable final Session session : sessions) {
            if (session == null) continue;
            result.add(new SessionDTO(session));
        }
        return result;
    }

    public SessionDTO(@Nullable final Session session) {
        if (session == null) return;
        id = session.getId();
        startTime = session.getStartTime();
        signature = session.getSignature();
        if (session.getUser() != null) userId = session.getUser().getId();
    }

}
