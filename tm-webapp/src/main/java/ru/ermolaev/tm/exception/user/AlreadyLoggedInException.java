package ru.ermolaev.tm.exception.user;

import ru.ermolaev.tm.exception.AbstractException;

public final class AlreadyLoggedInException extends AbstractException {

    public AlreadyLoggedInException() {
        super("Error! You are already logged in. Do logout to login again.");
    }

}
