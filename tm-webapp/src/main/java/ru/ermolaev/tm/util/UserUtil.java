package ru.ermolaev.tm.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import ru.ermolaev.tm.exception.user.AccessDeniedException;

public class UserUtil {

    static public User getAuthUser() throws AccessDeniedException {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException();
        return (User) principal;
    }

}
