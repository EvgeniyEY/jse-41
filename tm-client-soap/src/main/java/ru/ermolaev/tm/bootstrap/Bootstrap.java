package ru.ermolaev.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.exception.EmptyCommandException;
import ru.ermolaev.tm.util.TerminalUtil;

import java.lang.Exception;

@Component
public class Bootstrap {

    private final ApplicationEventPublisher publisher;

    @Autowired
    public Bootstrap(final ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    @SneakyThrows
    public void run(@Nullable final String[] args) {
        System.out.println("Welcome to task manager");
        if (parseArgs(args)) System.exit(0);
        String command = "";
        while (true) {
            command = TerminalUtil.nextLine();
            try {
                if (command == null || command.isEmpty()) throw new EmptyCommandException();
                publisher.publishEvent(new ConsoleEvent(command));
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        if (arg == null) return false;
        try {
            publisher.publishEvent(new ConsoleEvent(arg));
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("[FAIL]");
        }
        return true;
    }

}