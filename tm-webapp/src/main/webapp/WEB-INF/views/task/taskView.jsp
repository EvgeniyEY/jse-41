<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../resources/_prePage.jsp"/>

<p class="font" style="margin: 0px">TASK INFO</p>

<div style="margin-top: 5px">
    <label>ID:</label>
    <input type="text" name="id" readonly="true" size="50px" value=<c:out value="${task.id}"/>><br/>
</div>

<div style="margin-top: 5px">
    <label style="margin-top: 5px">Name:</label>
    <input type="text" name="name" readonly="true" value=<c:out value="${task.name}"/>><br/>
</div>

<div style="margin-top: 5px">
    <label style="margin-top: 5px">Description:</label>
    <input type="text" name="description" readonly="true" value=<c:out value="${task.description}"/>><br/>
</div>

<div style="margin-top: 5px">
    <label style="margin-top: 5px">Creation date:</label>
    <input type="date" name="creationDate" readonly="true" value=<c:out value="${task.creationDate}"/>><br/>
</div>

<div style="margin-top: 5px">
    <label style="margin-top: 5px">Start date:</label>
    <input type="date" name="startDate" readonly="true" value=<c:out value="${task.startDate}"/>><br/>
</div>

<div style="margin-top: 5px">
    <label style="margin-top: 5px">Complete date:</label>
    <input type="date" name="completeDate" readonly="true" value=<c:out value="${task.completeDate}"/>><br/>
</div>

<div style="margin-top: 5px">
    <label style="margin-top: 5px">Project ID:</label>
    <input type="text" name="user" readonly="true" value=<c:out value="${task.projectId}"/>><br/>
</div>

<div style="margin-top: 5px">
    <a class="button" href="/tasks/update/${task.id}">Go to update</a>
</div>

<div style="margin-top: 5px">
    <a class="button" href="/tasks/show">Go to task list</a>
</div>

<jsp:include page="../resources/_postPage.jsp"/>