package ru.ermolaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.User;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    Project getOneById(@Nullable String id) throws Exception;

    @Nullable
    Project createProject(@Nullable User user, @Nullable ProjectDTO projectDTO) throws Exception;

    @Nullable
    Project updateById(@Nullable User user, @Nullable ProjectDTO projectDTO) throws Exception;

    void updateStartDate(@Nullable User user, @Nullable ProjectDTO projectDTO) throws Exception;

    void updateCompleteDate(@Nullable User user, @Nullable ProjectDTO projectDTO) throws Exception;

    @NotNull
    Long countAllProjects();

    @NotNull
    Long countUserProjects(@Nullable User user) throws Exception;

    @Nullable
    Project findOneById(@Nullable String id) throws Exception;

    @Nullable
    Project findOneById(@Nullable User user, @Nullable String id) throws Exception;

    @Nullable
    Project findOneByName(@Nullable User user, @Nullable String name) throws Exception;

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAllByUserId(@Nullable User user) throws Exception;

    void removeOneById(@Nullable String id) throws Exception;

    void removeOneById(@Nullable User user, @Nullable String id) throws Exception;

    void removeOneByName(@Nullable User user, @Nullable String name) throws Exception;

    void removeAll();

    void removeAllByUserId(@Nullable User user) throws Exception;

}
