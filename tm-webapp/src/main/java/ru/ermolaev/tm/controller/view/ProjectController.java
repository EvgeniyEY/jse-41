package ru.ermolaev.tm.controller.view;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.ProjectDTO;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    private final IProjectService projectService;

    private final IUserService userService;

    private final ITaskService taskService;

    @Autowired
    public ProjectController(
            @NotNull final IProjectService projectService,
            @NotNull final IUserService userService,
            @NotNull final ITaskService taskService
    ) {
        this.projectService = projectService;
        this.userService = userService;
        this.taskService = taskService;
    }

    @GetMapping("/show")
    public ModelAndView show(@AuthenticationPrincipal final User user) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/projectList");
        modelAndView.addObject("projects", projectService.findAllByUserId(user));
        return modelAndView;
    }

    @GetMapping("/view/{id}")
    public ModelAndView view(
            @PathVariable("id") @NotNull final String id,
            @AuthenticationPrincipal final User user
    ) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/projectView");
        @Nullable final ProjectDTO projectDTO = ProjectDTO.toDTO(projectService.findOneById(user, id));
        if (projectDTO == null) return null;
        modelAndView.addObject("project", projectDTO);
        modelAndView.addObject("creator", user.getUsername());
        modelAndView.addObject("tasks", taskService.findAllByUserIdAndProjectId(user, id));
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/projectCreate");
        return modelAndView;
    }

    @PostMapping("/create")
    public ModelAndView create(
            @ModelAttribute("project") @NotNull final ProjectDTO projectDTO,
            @AuthenticationPrincipal final User user
    ) throws Exception {
        projectService.createProject(user, projectDTO);
        return new ModelAndView("redirect:/projects/show");
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(
            @PathVariable(value = "id") @NotNull final String id,
            @AuthenticationPrincipal final User user
    ) throws Exception {
        projectService.removeOneById(user, id);
        return new ModelAndView("redirect:/projects/show");
    }

    @GetMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id,
            @AuthenticationPrincipal final User user
    ) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/projectUpdate");
        @Nullable final ProjectDTO projectDTO = ProjectDTO.toDTO(projectService.findOneById(user, id));
        if (projectDTO == null) return null;
        modelAndView.addObject("project", projectDTO);
        modelAndView.addObject("users", userService.findAll());
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id,
            @ModelAttribute("project") @NotNull final ProjectDTO projectDTO,
            @AuthenticationPrincipal final User user
    ) throws Exception {
        projectDTO.setId(id);
        projectService.updateById(user, projectDTO);
        return new ModelAndView("redirect:/projects/show");
    }

}
