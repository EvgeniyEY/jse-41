package io.swagger.tmclient.command.task;

import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.TaskDTO;
import io.swagger.tmclient.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TaskListShowCommand extends AbstractCommand {

    @Autowired
    public TaskListShowCommand(@NotNull final DefaultApi api) {
        super(api);
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        @Nullable final List<TaskDTO> tasks = api.findAll_0();
        if (tasks == null) return;
        for (@NotNull final TaskDTO task: tasks) {
            System.out.println((tasks.indexOf(task) + 1)
                    + ". {id: "
                    + task.getId()
                    + "; name: "
                    + task.getName()
                    + "; description: "
                    + task.getDescription()
                    + "}");
        }
        System.out.println("[COMPLETE]");
    }

}
