package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.UserDTO;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.repository.IUserRepository;
import ru.ermolaev.tm.util.HashUtil;

import java.util.List;

@Service
public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final PasswordEncoder passwordEncoder
    ) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Nullable
    @Override
    public User getOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = userRepository.getOne(id);
        return user;
    }

    @Nullable
    @Override
    public User findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = userRepository.findById(id).orElse(null);
        return user;
    }

    @Nullable
    @Override
    public User findOneByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        return user;
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        @NotNull final List<User> users = userRepository.findAll();
        return UserDTO.toDTO(users);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeOneByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteByLogin(login);
    }

    @Override
    @Transactional
    public void removeAll() {
        userRepository.deleteAll();
    }

    @Override
    @Transactional
    public void create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String saltPass = HashUtil.hidePassword(password);
        if (saltPass == null) return;
        user.setPasswordHash(saltPass);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        @NotNull final String encodedPass = passwordEncoder.encode(password);
        user.setPasswordHash(encodedPass);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        @NotNull final String encodedPass = passwordEncoder.encode(password);
        user.setPasswordHash(encodedPass);
        user.setRole(role);
        userRepository.save(user);
    }

    @NotNull
    @Override
    public Long count() {
        @NotNull final Long count = userRepository.count();
        return count;
    }

    @Override
    @Transactional
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String newPassword
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        @Nullable final String saltPass = HashUtil.hidePassword(newPassword);
        if (saltPass == null) return;
        @Nullable final User user = findOneById(userId);
        if (user == null) return;
        user.setPasswordHash(saltPass);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updateUserFirstName(
            @Nullable final String userId,
            @Nullable final String newFirstName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyFirstNameException();
        @Nullable final User user = findOneById(userId);
        if (user == null) return;
        user.setFirstName(newFirstName);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updateUserMiddleName(
            @Nullable final String userId,
            @Nullable final String newMiddleName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new EmptyMiddleNameException();
        @Nullable final User user = findOneById(userId);
        if (user == null) return;
        user.setMiddleName(newMiddleName);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updateUserLastName(
            @Nullable final String userId,
            @Nullable final String newLastName
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newLastName == null || newLastName.isEmpty()) throw new EmptyLastNameException();
        @Nullable final User user = findOneById(userId);
        if (user == null) return;
        user.setLastName(newLastName);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updateUserEmail(
            @Nullable final String userId,
            @Nullable final String newEmail
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newEmail == null || newEmail.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findOneById(userId);
        if (user == null) return;
        user.setEmail(newEmail);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findOneByLogin(login);
        if (user == null) return;
        user.setLocked(true);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findOneByLogin(login);
        if (user == null) return;
        user.setLocked(false);
        userRepository.save(user);
    }

}
