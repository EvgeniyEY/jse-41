package ru.ermolaev.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.dto.TaskDTO;
import ru.ermolaev.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping(value = "/api/rest/task")
public class TaskRestEndpoint {

    private final ITaskService taskService;

    @Autowired
    public TaskRestEndpoint(
            @NotNull final ITaskService taskService
    ) {
        this.taskService = taskService;
    }

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO createTask(
            @RequestBody @Nullable final TaskDTO taskDTO
    ) throws Exception {
        return TaskDTO.toDTO(taskService.createTask(UserUtil.getAuthUser(), taskDTO));
    }

    @PutMapping(value = "/updateById", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO updateById(
            @RequestBody @Nullable final TaskDTO taskDTO
    ) throws Exception {
        return TaskDTO.toDTO(taskService.updateById(UserUtil.getAuthUser(), taskDTO));
    }

    @NotNull
    @GetMapping(value = "/countAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public Long countAllTasks() throws Exception {
        return taskService.countByUserId(UserUtil.getAuthUser());
    }

    @Nullable
    @GetMapping(value = "/findById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO findById(
            @PathVariable("id") @Nullable final String id
    ) throws Exception {
        return TaskDTO.toDTO(taskService.findOneById(UserUtil.getAuthUser(), id));
    }

    @NotNull
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> findAll() throws Exception {
        return taskService.findAllByUserId(UserUtil.getAuthUser());
    }

    @DeleteMapping(value = "/removeById/{id}")
    public void removeOneById(
            @PathVariable("id") @Nullable final String id
    ) throws Exception {
        taskService.removeOneById(UserUtil.getAuthUser(), id);
    }

}
