package ru.ermolaev.tm.controller.view;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.dto.TaskDTO;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    private final ITaskService taskService;

    private final IProjectService projectService;

    @Autowired
    public TaskController(
            @NotNull final ITaskService taskService,
            @NotNull final IProjectService projectService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @GetMapping("/show")
    public ModelAndView show(
            @AuthenticationPrincipal final User user
    ) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskList");
        modelAndView.addObject("tasks", taskService.findAllByUserId(user));
        return modelAndView;
    }

    @GetMapping("/view/{id}")
    public ModelAndView view(
            @PathVariable("id") @NotNull final String id,
            @AuthenticationPrincipal final User user
    ) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskView");
        @Nullable final TaskDTO taskDTO = TaskDTO.toDTO(taskService.findOneById(user, id));
        if (taskDTO == null) return null;
        modelAndView.addObject("task", taskDTO);
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create(
            @AuthenticationPrincipal final User user
    ) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskCreate");
        modelAndView.addObject("projects", projectService.findAllByUserId(user));
        return modelAndView;
    }

    @PostMapping("/create")
    public ModelAndView create(
            @ModelAttribute("task") @NotNull final TaskDTO taskDTO,
            @AuthenticationPrincipal final User user
    ) throws Exception {
        taskService.createTask(user, taskDTO);
        return new ModelAndView("redirect:/tasks/show");
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(
            @PathVariable(value = "id") @NotNull final String id,
            @AuthenticationPrincipal final User user
    ) throws Exception {
        taskService.removeOneById(user, id);
        return new ModelAndView("redirect:/tasks/show");
    }

    @GetMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id,
            @AuthenticationPrincipal final User user
    ) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskUpdate");
        @Nullable final TaskDTO taskDTO = TaskDTO.toDTO(taskService.findOneById(user, id));
        if (taskDTO == null) return null;
        modelAndView.addObject("task", taskDTO);
        modelAndView.addObject("projects", projectService.findAllByUserId(user));
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id,
            @ModelAttribute("task") @NotNull final TaskDTO taskDTO,
            @AuthenticationPrincipal final User user
    ) throws Exception {
        taskDTO.setId(id);
        taskService.updateById(user, taskDTO);
        return new ModelAndView("redirect:/tasks/show");
    }

}