<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../resources/_prePage.jsp"/>

<p class="font" style="margin: 0px">PROJECT INFO</p>

<div style="margin-top: 5px">
    <label>ID:</label>
    <input type="text" name="id" readonly="true" size="50px" value=<c:out value="${project.id}"/>><br/>
</div>

<div style="margin-top: 5px">
    <label style="margin-top: 5px">Name:</label>
    <input type="text" name="name" readonly="true" value=<c:out value="${project.name}"/>><br/>
</div>

<div style="margin-top: 5px">
    <label style="margin-top: 5px">Description:</label>
    <input type="text" name="description" readonly="true" value=<c:out value="${project.description}"/>><br/>
</div>

<div style="margin-top: 5px">
    <label style="margin-top: 5px">Creation date:</label>
    <input type="date" name="creationDate" readonly="true" value=<c:out value="${project.creationDate}"/>><br/>
</div>

<div style="margin-top: 5px">
    <label style="margin-top: 5px">Start date:</label>
    <input type="date" name="startDate" readonly="true" value=<c:out value="${project.startDate}"/>><br/>
</div>

<div style="margin-top: 5px">
    <label style="margin-top: 5px">Complete date:</label>
    <input type="date" name="completeDate" readonly="true" value=<c:out value="${project.completeDate}"/>><br/>
</div>

<div style="margin-top: 5px">
    <label style="margin-top: 5px">Creator:</label>
    <input type="text" name="user" readonly="true" value=<c:out value="${creator}"/>><br/>
</div>

<div style="margin-top: 5px">Project's tasks:
    <table border="1" width="50%" margin-top="0px">
        <tr>
            <td class="table-head-p" width="40%">ID</td>
            <td class="table-head-p" width="30%">Task Name</td>
            <td class="table-head-p" width="30%">Description</td>
        </tr>
        <c:forEach items="${tasks}" var="task">
            <tr>
                <td class="td_lp"><a href="/tasks/view/${task.id}"><c:out value="${task.id}"/></a></td>
                <td class="td_lp"><c:out value="${task.name}"/></td>
                <td class="td_lp"><c:out value="${task.description}"/></td>
            </tr>
        </c:forEach>
    </table>
</div>

<div style="margin-top: 5px">
    <a class="button" href="/projects/update/${project.id}">Go to update</a>
</div>

<div style="margin-top: 5px">
    <a class="button" href="/projects/show">Go to project list</a>
</div>

<jsp:include page="../resources/_postPage.jsp"/>