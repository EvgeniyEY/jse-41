package ru.ermolaev.tm.api.service;

public interface IPropertyService {

    String getServerHost();

    Integer getServerPort();

    String getSessionSalt();

    Integer getSessionCycle();

    String getDatabaseDriver();

    String getDatabaseType();

    String getDatabaseHost();

    String getDatabasePort();

    String getDatabaseName();

    String getDatabaseUsername();

    String getDatabasePassword();

    String getJdbcUrl();

}
