package io.swagger.tmclient.exception;

import org.jetbrains.annotations.NotNull;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! This argument does not exist.");
    }

    public UnknownArgumentException(@NotNull final String argument) {
        super("Error! This argument [" + argument + "] does not exist.");
    }

}
