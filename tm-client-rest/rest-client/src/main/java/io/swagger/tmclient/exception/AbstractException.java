package io.swagger.tmclient.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractException extends Exception {

    public AbstractException(@NotNull final String message) {
        super(message);
    }

}
