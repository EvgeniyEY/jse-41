package io.swagger.tmclient.command.project;

import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.ProjectDTO;
import io.swagger.tmclient.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProjectListShowCommand extends AbstractCommand {

    @Autowired
    public ProjectListShowCommand(@NotNull final DefaultApi api) {
        super(api);
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS LIST]");
        @NotNull final List<ProjectDTO> projects = api.findAll();
        if (projects == null) return;
        for (@NotNull final ProjectDTO project: projects) {
            System.out.println((projects.indexOf(project) + 1)
                    + ". {id: "
                    + project.getId()
                    + "; name: "
                    + project.getName()
                    + "; description: "
                    + project.getDescription()
                    + "}");
        }
        System.out.println("[COMPLETE]");
    }

}
