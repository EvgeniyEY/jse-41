package io.swagger.tmclient.command.task;

import io.swagger.client.api.DefaultApi;
import io.swagger.tmclient.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.swagger.tmclient.util.TerminalUtil;

@Component
public class TaskRemoveByIdCommand extends AbstractCommand {

    @Autowired
    public TaskRemoveByIdCommand(@NotNull final DefaultApi api) {
        super(api);
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        api.removeOneById(id);
        System.out.println("[OK]");
    }

}
