package ru.ermolaev.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class ProjectSoapEndpoint {

    @Autowired
    private IProjectService projectService;

    @Nullable
    @WebMethod
    public ProjectDTO createProject(
            @WebParam(name = "projectDTO", partName = "projectDTO") @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        return ProjectDTO.toDTO(projectService.createProject(UserUtil.getAuthUser(), projectDTO));
    }

    @Nullable
    @WebMethod
    public ProjectDTO updateProjectById(
            @WebParam(name = "projectDTO", partName = "projectDTO") @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        return ProjectDTO.toDTO(projectService.updateById(UserUtil.getAuthUser(), projectDTO));
    }

    @NotNull
    @WebMethod
    public Long countAllProjects() throws Exception {
        return projectService.countUserProjects(UserUtil.getAuthUser());
    }

    @Nullable
    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        return ProjectDTO.toDTO(projectService.findOneById(UserUtil.getAuthUser(), id));
    }

    @NotNull
    @WebMethod
    public List<ProjectDTO> findAllProjects() throws Exception {
        return projectService.findAllByUserId(UserUtil.getAuthUser());
    }

    @WebMethod
    public void removeProjectById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        projectService.removeOneById(UserUtil.getAuthUser(), id);
    }

}
