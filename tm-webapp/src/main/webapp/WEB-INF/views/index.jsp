<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:include page="resources/_prePage.jsp" />

    <h1>Welcome to task manager!</h1>
<sec:authorize access="!isAuthenticated()">
    <h2><a class="button" href="/login">Login</a></h2>
</sec:authorize>
<sec:authorize access="isAuthenticated()">
    <h2><a class="button" href="/logout">Logout</a></h2>
</sec:authorize>

<jsp:include page="resources/_postPage.jsp" />