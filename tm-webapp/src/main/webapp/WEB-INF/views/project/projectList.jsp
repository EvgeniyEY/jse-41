<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../resources/_prePage.jsp"/>

<p class="font" style="margin: 0px">PROJECT LIST</p>

<table border="1" width="100%" margin-top="0px">
    <tr>
        <td class="table-head" width="25%">ID</td>
        <td class="table-head" width="20%">Project Name</td>
        <td class="table-head" width="31%">Description</td>
        <td class="table-head" width="8%">View</td>
        <td class="table-head" width="8%">Update</td>
        <td class="table-head" width="8%">Remove</td>
    </tr>
    <c:forEach items="${projects}" var="project">
        <tr>
            <td class="td_list"><c:out value="${project.id}"/></td>
            <td class="td_list"><c:out value="${project.name}"/></td>
            <td class="td_list"><c:out value="${project.description}"/></td>
            <td style="font-size: 20px" align="center"><a href="/projects/view/${project.id}">View</a></td>
            <td style="font-size: 20px" align="center"><a href="/projects/update/${project.id}">Update</a></td>
            <td style="font-size: 20px" align="center"><a href="/projects/remove/${project.id}">Remove</a></td>
        </tr>
    </c:forEach>
</table>
<div style="margin-top: 10px;">
    <a href="/projects/create" class="button">Create new project</a>
</div>
<div style="margin-top: 10px;">
    <a href="/" class="button">Main page</a>
</div>

<jsp:include page="../resources/_postPage.jsp"/>