package ru.ermolaev.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public class AuthenticationSoapEndpoint {

    @Autowired
    private AuthenticationManager authenticationManager;

    @WebMethod
    public boolean login(
            @WebParam(name = "username") final String username,
            @WebParam(name = "password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return true;
        } catch (final Exception e) {
            return false;
        }
    }

    @WebMethod
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

}
