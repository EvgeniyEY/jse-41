<jsp:include page="resources/_prePage.jsp" />

<h2>Login in Task Manager</h2>
<h3>Enter your Username and Password</h3>
<form name='f' action='/login' method='POST'>
    <table>
        <tr>
            <td>User:</td>
            <td><input type='text' name='username' value=''/></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type='password' name='password'/></td>
        </tr>
        <tr>
            <td colspan='2'><input class="button" name="submit" type="submit" value="Login"/></td>
        </tr>
    </table>


<jsp:include page="resources/_postPage.jsp" />
