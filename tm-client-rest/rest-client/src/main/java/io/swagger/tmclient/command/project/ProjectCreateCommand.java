package io.swagger.tmclient.command.project;

import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.ProjectDTO;
import io.swagger.tmclient.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.swagger.tmclient.util.TerminalUtil;

@Component
public class ProjectCreateCommand extends AbstractCommand {

    @Autowired
    public ProjectCreateCommand(@NotNull final DefaultApi api) {
        super(api);
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create a new project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        api.createProject(projectDTO);
        System.out.println("[COMPLETE]");
    }

}
