package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;

public class ProjectRestControllerTest {

    private final DefaultApi api = new DefaultApi();

    @Test
    public void integrationProjectTest() throws ApiException {
        final int startValueCount = api.countAllProjects().intValue();
        final int startValueFindAll = api.findAll().size();

        @NotNull ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId("e4b137a5-e090-4c42-b68d-cbfc5350e0fe");
        projectDTO.setName("rest-test-project-name");
        projectDTO.setDescription("rest-test-project-description");
        @NotNull final ProjectDTO createdProject = api.createProject(projectDTO);

        Assert.assertEquals(startValueCount + 1, api.countAllProjects().intValue());
        Assert.assertEquals(startValueFindAll + 1, api.findAll().size());

        @NotNull final String newName = "UPDATED-rest-test-project-name";
        @NotNull final String newDescription = "UPDATED-rest-test-project-description";

        createdProject.setName(newName);
        createdProject.setDescription(newDescription);
        api.updateById(createdProject);

        @NotNull final ProjectDTO findProject = api.findById(createdProject.getId());
        Assert.assertNotNull(findProject);
        Assert.assertEquals(findProject.getName(), newName);
        Assert.assertEquals(findProject.getDescription(), newDescription);

        api.removeById(createdProject.getId());
        Assert.assertEquals(startValueCount, api.countAllProjects().intValue());
        Assert.assertEquals(startValueFindAll, api.findAll().size());
    }

}
